const express = require('express');
const app = express();
// database connection
const mongoose = require("mongoose");
const db = mongoose.connect("mongodb://127.0.0.1:27017/todo-api");

// const {
//   taskPatch,
//   taskPost,
//   taskGet,
//   taskDelete,
// } = require("./controllers/taskController.js");
const {
sessionGet,
sessionPost
} = require("./controllers/sessionController.js");
const {
  userPatch,
  userPost,
  userGet,
  userDelete,
  userLogin
} = require("./controllers/userController.js");

// parser for the request body (required for the POST and PUT methods)
const bodyParser = require("body-parser");
app.use(bodyParser.json());

// check for cors
const cors = require("cors");
app.use(cors({
  domains: '*',
  methods: "*"
}));


// listen to the task request
// app.get("/api/tasks", taskGet);
// app.post("/api/tasks", taskPost);
// app.patch("/api/tasks", taskPatch);
// app.put("/api/tasks", taskPatch);
// app.delete("/api/tasks", taskDelete);

// listen to the user request
app.get("/api/users", userGet);
app.get("/api/users/login", userLogin);
app.post("/api/users", userPost);
app.patch("/api/users", userPatch);
app.put("/api/users", userPatch);
app.delete("/api/users", userDelete);

app.get("/api/sessions", sessionGet);
app.post("/api/sessions", sessionPost);




app.listen(3000, () => console.log(`Example app listening on port 3000!`))
