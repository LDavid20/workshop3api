const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const session = new Schema({
  username: { type: String },
  sessionID: { type: String },
  started: { type: Date, default: Date.now() }
});

module.exports = mongoose.model('sessions', session);