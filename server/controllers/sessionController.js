const Session = require("../models/sessionModel");
const User = require("../models/userModel");
const e = require("express");

/*Creado una session*
 * 
 * @param {*} req 
 * @param {*} res 
 */
const sessionPost = (req, res) => {
    var session = new Session();
  
    session.username = req.body.username;
    session.sessionID = req.body.sessionID;

    
  
    if (session.username && session.sessionID ) {
      session.save(function (err) {
        if (err) {
          res.status(422);
          console.log('Error while saving the session', err)
          res.json({
            error: 'There was an error saving the session'
          });
        }
        console.log("AQUI");
        res.status(201);//CREATED
        res.header({
          'location': `http://localhost:3000/api/sessions/?id=${session.id}`
        });
        res.json(session);
      });
    } else {
      res.status(422);
      console.log('Error while saving the session')
      res.json({
        error: 'No valid data provided for session'
      });
    }
  };

/*Obteniendo el usuario por la session*
 * 
 * @param {*} req 
 * @param {*} res 
 */
const sessionGet = (req, res) => {
    // if an specific user is required
let sessionID = req.query.sessionID;

    if (req.query && sessionID) {
      Session.findOne({sessionID: sessionID}, function (err, session) {
        if (err) {
          res.status(404);
          console.log('Error while queryting the session', err)
          res.json({ error: "Session doesnt exist" })
        }
        User.findById(sessionID, function (err, user) {
            if (err) {
              res.status(404);
              console.log('Error while queryting the user', err)
              res.json({ error: "User doesnt exist" })
            }
            res.json(user);
          });
      });
    } 

    // {
    //   "sessionID": "5f3078c94a9af53050e3debd"
      
    // }
    // else {
    //   // get all users
    //   User.find(function (err, users) {
    //     if (err) {
    //       res.status(422);
    //       res.json({ "Error": err });
    //     }
    //     res.json(users);
    //   });
  
    // }
  };

  module.exports = {
    sessionGet,
    sessionPost,
    // userPatch,
    // userDelete,
    // userLogin
  }