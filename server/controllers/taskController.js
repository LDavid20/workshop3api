const Task = require("../models/taskModel");

/**
 * Creates a task
 *
 * @param {*} req
 * @param {*} res
 */
const taskPost = (req, res) => {
  var task = new Task();

  task.title = req.body.title;
  task.detail = req.body.detail;

  if (task.title && task.detail) {
    task.save(function (err) {
      if (err) {
        res.status(422);
        console.log('Error while saving the task', err)
        res.json({
          error: 'There was an error saving the task'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/tasks/?id=${task.id}`
      });
      res.json(task);
    });
  } else {
    res.status(422);
    console.log('Error while saving the task')
    res.json({
      error: 'No valid data provided for task'
    });
  }
};

/**
 * Get all tasks
 *
 * @param {*} req
 * @param {*} res
 */
const taskGet = (req, res) => {
  // if an specific task is required
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the task', err)
        res.json({ error: "Task doesnt exist" })
      }
      res.json(task);
    });
  } else {
    // get all tasks
    Task.find(function (err, tasks) {
      if (err) {
        res.status(422);
        res.json({ "Error": err });
      }
      res.json(tasks);
    });

  }
};

/**
 * Updates a task
 *
 * @param {*} req
 * @param {*} res
 */
const taskPatch = (req, res) => {
  // get task by id
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the task', err)
        res.json({ error: "Task doesnt exist" })
      }

      task.title = req.body.title ? req.body.title : task.title;
      task.detail = req.body.detail ? req.body.detail : task.detail;

      task.save(function (err) {
        if (err) {
          res.status(422);
          console.log('Error while saving the task', err)
          res.json({
            error: 'There was an error saving the task'
          });
        }
        res.status(200); // OK
        res.json(task);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "Task doesnt exist" })
  }
};


/**
 * Delete a task
 *
 * @param {*} req
 * @param {*} res
 */

const taskDelete = (req, res) => {
  if (req.query && req.query.id) {
    Task.findById(req.query.id, function (err, task) {
      if (err) {
        res.status(404);
        console.log('error while queryting the task', err)
        res.json({ error: "Task doesnt exist" })
      }
      
      
        
        task.remove(function (err){
          if(err){
            res.status(500).send({message: 'Error al borrar el task: ${err}'})
          } 
          res.status(200).send({message: 'El task ha sido eliminado'})

        })
      
     
    });
  } else {
    res.status(404);
    res.json({ error: "Task doesnt exist" })
  }
};

module.exports = {
  taskGet,
  taskPost,
  taskPatch,
  taskDelete
}