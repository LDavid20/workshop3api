  const User = require("../models/userModel");
const e = require("express");

/**
 * Creates a user
 *
 * @param {*} req
 * @param {*} res
 */
const userPost = (req, res) => {
  var user = new User();

  user.firstname = req.body.firstname;
  user.lastname = req.body.lastname;
  user.user = req.body.user;
  user.password = req.body.password;
  

  if (user.user && user.password && user.firstname && user.lastname ) {
    user.save(function (err) {
      if (err) {
        res.status(422);
        console.log('Error while saving the user', err)
        res.json({
          error: 'There was an error saving the user'
        });
      }
      res.status(201);//CREATED
      res.header({
        'location': `http://localhost:3000/api/users/?id=${user.id}`
      });
      res.json(user);
    });
  } else {
    res.status(422);
    console.log('Error while saving the user')
    res.json({
      error: 'No valid data provided for user'
    });
  }
};

/**
 * Get all users
 *
 * @param {*} req
 * @param {*} res
 */
const userGet = (req, res) => {
  // if an specific user is required
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the user', err)
        res.json({ error: "User doesnt exist" })
      }
      res.json(user);
    });
  } else {
    // get all users
    User.find(function (err, users) {
      if (err) {
        res.status(422);
        res.json({ "Error": err });
      }
      res.json(users);
    });

  }
};

/**
 * Updates a student
 *
 * @param {*} req
 * @param {*} res
 */
const userPatch = (req, res) => {
  // get user by id
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the user', err)
        res.json({ error: "User doesnt exist" })
      }

      user.user = req.body.user ? req.body.user : user.user;
      user.password = req.body.password ? req.body.password : user.password;
      user.firstname = req.body.firstname ? req.body.firstname : user.firstname;
      user.lastname = req.body.lastname ? req.body.lastname : user.lastname;

      user.save(function (err) {
        if (err) {
          res.status(422);
          console.log('Error while saving the user', err)
          res.json({
            error: 'There was an error saving the user'
          });
        }
        res.status(200); // OK
        res.json(user);
      });
    });
  } else {
    res.status(404);
    res.json({ error: "User doesnt exist" })
  }
};

/**
 * Delete a user
 *
 * @param {*} req
 * @param {*} res
 */

const userDelete = (req, res) => {
  if (req.query && req.query.id) {
    User.findById(req.query.id, function (err, user) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the user', err)
        res.json({ error: "User doesnt exist" })
      }
      
      
        
      user.remove(function (err){
          if(err){
            res.status(500).send({message: 'Error al borrar el user: ${err}'})
          } 
          res.status(200).send({message: 'El user ha sido eliminado'})

        })
      
     
    });
  } else {
    res.status(404);
    res.json({ error: "User doesnt exist" })
  }
};


/**
 * Get login users
 *
 * @param {*} req
 * @param {*} res
 */
const userLogin = (req, res) => {
  if (req.query && req.query.user && req.query.password) {
    User.find({'user': req.query.user,'password': req.query.password}, function (err, user) {
      if (err) {
        res.status(404);
        console.log('Error while queryting the user', err)
        res.json({ error: "User doesnt exist 1" })
      }
      else if(user.toString().length <=2){
        res.status(404);
        // console.log('Error while queryting the user', err)
        res.json({ error: "Usuario o Contraseña invalida" })
        // res.json({ error: "User doesnt exist 3" })
      }
      else{
        res.json({message: "Inicio de seccion exitoso"});
        // res.json(user);
      }

      
     
    });
  } else {
    res.status(404);
    res.json({ error: "User doesnt exist 2" })
  }
};

module.exports = {
  userGet,
  userPost,
  userPatch,
  userDelete,
  userLogin
}